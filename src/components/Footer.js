import React from "react";
import { FaTwitter, FaFacebookF, FaLinkedinIn } from "react-icons/fa";
import "../css/footer.css";

const Footer = () => {
  return (
    <section className="footer">
      <div className="footerStay">
      <p>
        <span>651-279-8985</span>
      </p>
      <div className="icons">
        <a href=""><FaTwitter id="twitter" /></a>
        <a href=""><FaFacebookF id="facebook"/></a>
        <a href=""><FaLinkedinIn id="linkedIn"/></a>
      </div>
      <span>©2021 by Bitslogical.</span>
      </div>
    </section>
  );
};

export default Footer;
