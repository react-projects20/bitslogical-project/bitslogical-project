import  Button  from 'react-bootstrap/Button';


function BootButton (props) {
    return ( <Button variant={props.type} href={props.link} >{props.text}</Button> );
}
 
export default BootButton;

// TODO needs a generic style for padding and add link propsinst