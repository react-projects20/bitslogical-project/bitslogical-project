import React from 'react';
<<<<<<< HEAD:src/components/SideBar.js
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import SideBarItems from './SideBarItems';
import '../css/sidebar.css';
=======
import HomeIcon from "@material-ui/icons/Home";
import SideBarItems from './SideBarItems';
import InfoIcon from '@material-ui/icons/Info';
import RoomServiceIcon from '@material-ui/icons/RoomService';
import ContactSupportIcon from '@material-ui/icons/ContactSupport';
//import SettingsIcon from "@material-ui/icons/Settings";
import './sidebar.css';

//componenets
//css
>>>>>>> 5a8b350713796bdbb59f41927e5a02a8b0b66760:src/components/SideBar/SideBar.js

function onClick(e, item) {
    window.alert(JSON.stringify(item, null, 2));
  }

const items = [
    {  label: 'Home', href: '/Home', Icon: HomeIcon},
    { 
        label: 'About',
        Icon: InfoIcon,
        href: '/About',
    },
    {
        label: 'Services',
        href: '/Services',
        Icon: RoomServiceIcon,
        items: [
            { label: 'Something 1', href: '/Something' },
            { label: 'Something 2', href: '/Something'},
            { label: 'Something 3', href: '/Something'},
        ],
    },
    "divider",
    {
        label: 'Contact',
        Icon: ContactSupportIcon,
        href: '/Contact',
    },
    
]


function SideBar() {

    return (
        <div className=" col-md-2 col-sm-12">
            <h2 className="sidebar-title">SideBar Title</h2>
            
            <ul className="navbar-nav">
                <SideBarItems items={items} />
            </ul>
        </div>
    );

}


export default SideBar;

