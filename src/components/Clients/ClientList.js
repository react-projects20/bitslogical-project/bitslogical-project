import './client.css';


function ClientList() {

    return (
        <section className='client-list-section'>
            <div className='client-list-div-one'>
                <div className='client-list-div-two'>
                    <div className='client-list-div-three'></div>
                </div>
            </div>

            <div className='client-list-div-four'>
                <div className='client-list-div-five'></div>
                <div className='client-list-div-six'>
                    <div className='client-list-div-seven'>
                        <div className='client-list-div-eight'>
                            <div className='client-list-div-nine'>
                                <div className='client-list-div-ten'></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default ClientList